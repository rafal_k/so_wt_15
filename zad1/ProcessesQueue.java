package zad1;

public class ProcessesQueue
{
    private Element head = null;
    private Element tail = null;
    private int size = 0;

    public void enqueue(Process p)
    {
        Element el = new Element(p);
        if(head == null)
        {
            head = el;
            tail = el;
        }
        else
        {
            tail.next = el;
            tail = el;
        }

        size++;
    }

    public Process dequeue()
    {
        if(head == null)
            return null;

        Element newHead = head.next;
        Element e = head;
        head = newHead;

        if(tail == e)
            tail = null;

        size--;

        return e.value;
    }

    public Process peek()
    {
        if(head == null)
            return null;

        return head.value;
    }

    public int size()
    {
        return size;
    }

    class Element
    {
        Process value;
        Element next;

        public Element(Process value)
        {
            this.value = value;
            this.next = null;
        }

        public Element(Process value, Element next)
        {
            this.value = value;
            this.next = next;
        }

        public Process getValue()
        {
            return value;
        }

        public Element getNext()
        {
            return next;
        }
    }
}

