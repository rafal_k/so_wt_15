package zad1;

import java.util.ArrayList;

public class Timer {
    private ArrayList<ExecutedProcess> executions;
    private Processor processor;

    public Timer(Processor processor) {
        this.executions = new ArrayList<>();
        this.processor = processor;
    }



    public void addProcess(Process process){
        executions.add(new ExecutedProcess(process, this.getCurrentTime()-this.getRQTime(process)));
    }

    private int getRQTime(Process process){
        return process.getRequestTime();
    }

    private int getCurrentTime(){
        return processor.getCurrentTime();
    }

    public int averageTime(){
        int sum = 0;
        for (ExecutedProcess process : executions) {
            sum += process.getTime();
        }
        return sum/executions.size();
    }

    public ArrayList<ExecutedProcess> getExecutions() {
        return executions;
    }

    public void setExecutions(ArrayList<ExecutedProcess> executions) {
        this.executions = executions;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }
}
