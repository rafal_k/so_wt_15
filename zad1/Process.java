package zad1;

//Planowanie dostępu do procesora dla zgłaszających się procesów
//
//        Algorytmy:
//        - FCFS
//        - SJF (z wywłaszczeniem i bez)
//        - rotacyjne
//
//        Możliwe reprezentacja procesu:
//        - numer identyfikacyjny
//        - długość fazy procesora (mile widziany rozkład normalny)
//        - moment zgłoszenia się
//        - czas oczekiwania procesu na obsłużenie
//
//        Zadbać o to, aby te same dane testowe były używane do przetestowania wszystkich algorytmów
//        Określić liczbę ciągów testowych
//        Zgłaszanie się procesów powinno być losowe (muszą pojawiać sie kolejki)


public class Process
{
    protected int id;
    protected int resources;
    protected int remainingTime;
    protected int requestTime;

    public Process(int id, int resources, int requestTime)
    {
        this.id = id;
        this.resources = resources;
        remainingTime = resources;
        this.requestTime = requestTime;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getResources()
    {
        return resources;
    }

    public void setResources(int resources)
    {
        this.resources = resources;
    }

    public int getRemainingTime()
    {
        return remainingTime;
    }

    public void setRemainingTime(int remainingTime)
    {
        this.remainingTime = remainingTime;
    }

    public int getRequestTime()
    {
        return requestTime;
    }

    public void setRequestTime(int requestTime)
    {
        this.requestTime = requestTime;
    }
}
