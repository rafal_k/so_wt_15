package zad1;

import java.util.LinkedList;
import java.util.Random;

public class TestDataGenerator {
    private ProcessesQueue processes;
    private int numberOfProcesses;

    public TestDataGenerator(int numberOfProcesses) {
        this.processes = new ProcessesQueue();
        this.numberOfProcesses = numberOfProcesses;
    }

    public ProcessesQueue generate() {
        Random r = new Random();
        int requestTime = 0;
        for (int i = 0; i < numberOfProcesses; i++) {
            int resources = Math.abs( (int)(r.nextGaussian() * 10 + 25));
            requestTime += (int)(Math.random() * 7);
            Process process = new Process(i, resources, requestTime);
            processes.enqueue(process);
        }
        return processes;
    }
}
