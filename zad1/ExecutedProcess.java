package zad1;

public class ExecutedProcess {
    private Process process;
    private int time;

    public ExecutedProcess(Process process,int time){
        this.process = process;
        this.time = time;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
