/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad1;

/**
 *
 * @author stud
 */
public class Processor {
    
    private int currentTime;
    
    public Processor(){
        this.currentTime = 0;
    }
    
    public int getCurrentTime() {
        return currentTime;
    }
    
    public void setCurrentTime(int time) {
        this.currentTime = time;
    }
    
    void work(Process currentProcess) {
        currentTime += currentProcess.resources;
    }
}
