package zad2;
import java.util.*;
public class SCAN implements Strategy{

    
    private int currentTime;
    private Dysk dysk;
    private ArrayList<Task> forwardList = new ArrayList<>();
    private ArrayList<Task> backwardList = new ArrayList<>();
    private ArrayList<Task> tasksList;
    private int sumOfMoves = 0;
    private int currentMove = 0;

    SCAN(Dysk dysk, ArrayList<Task> tasksList) {
        this.dysk = dysk;
        this.tasksList = tasksList;
    }
    
    
    public int work() {
        
        while(!forwardList.isEmpty() || !backwardList.isEmpty() || !tasksList.isEmpty()) {
            for(int i = 0; i <= dysk.getMax(); i++) {
                if(!tasksList.isEmpty()) {
                    if(currentTime >= tasksList.get(0).getReqTime()) {
                        if(i <= tasksList.get(0).getPosition()) {
                            forwardList = insertNewTask(forwardList, tasksList.remove(0));
                        }
                        else {
                            backwardList = insertNewTask(backwardList, tasksList.remove(0));
                        }
                    }
                }
                if(!forwardList.isEmpty()) {
                    if(i == forwardList.get(0).getPosition()) {
                        forwardList.remove(0);
                        sumOfMoves += currentMove;
                        currentMove = 0;
                    }
                }
                currentMove++;
                currentTime++;
            }
            for(int i = dysk.getMax(); i >= 0; i--) {
                if(!tasksList.isEmpty()) {
                    if(currentTime >= tasksList.get(0).getReqTime()) {
                      if(i >= tasksList.get(0).getPosition()) {
                          backwardList = insertNewTask(backwardList, tasksList.remove(0));
                      }
                      else {
                          forwardList = insertNewTask(forwardList, tasksList.remove(0));
                      }
                  }
                }
                if(!backwardList.isEmpty()) {
                    if(i == backwardList.get(backwardList.size() - 1).getPosition()) {
                       backwardList.remove(0);
                       sumOfMoves += currentMove;
                       currentMove = 0;
                   }
                }
                currentMove++;
                currentTime++;
            }
        }
        return sumOfMoves;
    }
    
    private ArrayList<Task> insertNewTask(ArrayList<Task> arrayList, Task task) {
        if(arrayList.isEmpty()) {
            arrayList.add(task);
            return arrayList;
        }
        for(int i = 0; i < arrayList.size(); i++) {
            if(task.getPosition() < arrayList.get(i).getPosition()) {
                arrayList.add(i, task);
                  break;
            } 
        }
                return arrayList;
    }
}

