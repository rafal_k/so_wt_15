/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad2;


/**
 * @author stud
 */
public class Tester
{
    protected Dysk dysk;
    protected int sum;
    private Strategy strategy;

    public Tester(Dysk dysk, Strategy strategy)
    {
        this.dysk = dysk;
        this.strategy = strategy;
    }

    public void work()
    {
        sum = strategy.work();
    }

    public int getResults()
    {
        return sum;
    }
}
