package zad2;

public class Task {
    private int position;
    private int ID;
    private int reqTime;

    public Task(int ID, int position, int reqTime) {
        if (position < 0 || ID < 0 || reqTime < 0) throw new IllegalArgumentException();
        this.reqTime = reqTime;
        this.position = position;
        this.ID = ID;
    }

    public int getReqTime() {
        return reqTime;
    }

    public int getPosition() {
        return position;
    }

    public int getID() {
        return ID;
    }

    @Override
    public String toString() {
        return String.format("%5d. %5d %5d", ID, position, reqTime);
    }
}
