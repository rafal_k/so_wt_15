package zad2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {

    @SuppressWarnings("unchecked")
    public static <T extends List<Task>> T generate(int numberOfTasks, int max, int median, int stdev) {
        ArrayList<Task> tasksList = new ArrayList<>(numberOfTasks);

        int position, quantity;
        int reqTime = 0;
        int ID = 0;
        Random r = new Random();

        while (numberOfTasks > 0) {
            position = genPosition(max, median, stdev);

            do quantity = (int) (r.nextGaussian() * 1 + 2);
            while (quantity < 0);

            if (quantity > numberOfTasks) quantity = numberOfTasks;
            for (int i = 0; i < quantity; i++) {
                tasksList.add(new Task(ID, position, reqTime));
                ID++;
                position = genPosition(max, median, stdev);
                numberOfTasks--;
            }
            reqTime += (int) (Math.random() * 2 + 1);
        }

        return (T) tasksList;
    }

    private static int genPosition(int max, int median, int stdev) {
        Random r = new Random();
        int pos;

        do pos = (int) (r.nextGaussian() * median + stdev);
        while (pos < 0 || pos > max);

        return pos;
    }
}
