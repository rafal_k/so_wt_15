package zad2;

public interface Strategy {
    int work();
}
