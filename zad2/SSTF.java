package zad2;

import java.util.List;

public class SSTF implements Strategy
{
    protected Dysk disc;
    protected List<Task> queue;
    protected int currentTime = 0;

    public SSTF(Dysk disc, List<Task> queue)
    {
        this.disc = disc;
        this.queue = queue;
    }

    @Override
    public int work()
    {
        int sum = 0;
        Task task;
        while(queue.size() > 0)
        {
            if((task = nextTask()) != null)
            {
                sum += disc.moveTo(task.getPosition());
                queue.remove(task);
            }

            currentTime++;
        }
        disc.moveTo(0);
        return sum;
    }

    public Task nextTask()
    {
        Task min = null;
        int minPos = 0;
        for(Task t : queue)
        {
            if(t.getReqTime() <= currentTime)
            {
                if(min == null)
                {
                    min = t;
                    minPos = Math.abs(t.getPosition() - disc.getCurrentPos());
                }
                else if(Math.abs(t.getPosition() - disc.getCurrentPos()) < minPos)
                {
                    min = t;
                    minPos = Math.abs(t.getPosition() - disc.getCurrentPos());
                }
            }
        }

        return min;
    }

    public int getCurrentTime()
    {
        return currentTime;
    }
}
