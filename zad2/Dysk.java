package zad2;

public class Dysk {
    
    private int max;
    private int pos;

    public Dysk(int max) {
        if (max < 0) throw new IllegalArgumentException();
        this.max = max;
        pos = 0;
    }

    public int moveTo(int pos){
        if (pos > max || pos < 0) throw new IllegalArgumentException();
        int i = Math.abs(this.pos - pos);
        this.pos = pos;
        return i;
    }
    public int getCurrentPos(){
        return pos;
    }

    public int getMax() {
        return max;
    }
}
