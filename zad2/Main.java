package zad2;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Dysk dysk = new Dysk(200);
        ArrayList<Task> list = Generator.generate(100, dysk.getMax(), 40, 20);
        ArrayList<Task> list1 = new ArrayList<>();
        list1.addAll(list);
        ArrayList<Task> list2 = new ArrayList<>();
        list2.addAll(list);



        Tester tester = new Tester(dysk, new FCFS(dysk, list));
        tester.work();
        System.out.println("Suma przesunięć FCFS wynosi: " + tester.getResults());

        Tester tester1 = new Tester(dysk, new SSTF(dysk, list1));
        tester1.work();
        System.out.println("Suma przesunięć SSTF wynosi: " + tester1.getResults());


        Tester tester2 = new Tester(dysk, new SCAN(dysk, list2));
        tester2.work();
        System.out.println("Suma przesunięć SCAN wynosi: " + tester2.getResults());
    }
}
