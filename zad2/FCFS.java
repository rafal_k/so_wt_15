package zad2;

import java.util.List;

public class FCFS implements Strategy {
    private Dysk dysk;
    private List<Task> lista;
    private int pos;

    public FCFS(Dysk dysk, List<Task> lista){
        this.dysk = dysk;
        this.lista = lista;
        pos = 0;
    }

    public Task nextTask() {
        if (pos < lista.size()) {
            return lista.get(pos++);
        }
        else
            return null;
    }

    @Override
    public int work() {
        int sum = 0;
        Task task;
        while ((task = this.nextTask()) != null){
            sum += dysk.moveTo(task.getPosition());
        }
        dysk.moveTo(0);
        return sum;
    }
}
